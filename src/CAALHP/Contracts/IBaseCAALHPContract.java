package CAALHP.Contracts;

import java.util.AbstractMap;

/**
 * Created by rgst on 23-05-2014.
 */
public interface IBaseCAALHPContract {
    String GetName();
    Boolean IsAlive();
    void Notify(AbstractMap.SimpleEntry<String, String> notification);
    void ShutDown();
}
