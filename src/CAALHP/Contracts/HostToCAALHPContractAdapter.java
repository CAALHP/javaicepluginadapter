package CAALHP.Contracts;

import CAALHP.SOAICE.Contracts.IHostContractPrx;

import java.util.AbstractMap;

/**
 * Created by rgst on 23-05-2014.
 */
public class HostToCAALHPContractAdapter implements IHostCAALHPContract {
    private final IHostContractPrx hostContract;

    public HostToCAALHPContractAdapter(IHostContractPrx host)
    {
        System.out.println("Constructing HostToCAALHPContractAdapter");
        hostContract = host;
    }

    public void ReportEvent(AbstractMap.SimpleEntry<String, String> value)
    {
        hostContract.ReportEvent(value.getKey(), value.getValue());
    }

    public void SubscribeToEvents(String fullyQualifiedNameSpace, int processId)
    {
        hostContract.SubscribeToEvents(fullyQualifiedNameSpace, processId);
    }

    public void UnSubscribeToEvents(String fullyQualifiedNameSpace, int processId)
    {
        hostContract.UnSubscribeToEvents(fullyQualifiedNameSpace, processId);
    }
}
