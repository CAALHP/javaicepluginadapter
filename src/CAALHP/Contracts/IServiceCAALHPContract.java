package CAALHP.Contracts;

/**
 * Created by rgst on 27-05-2014.
 */
public interface IServiceCAALHPContract extends IBaseCAALHPContract {
    void Initialize(IServiceHostCAALHPContract hostObj, int processId);
    void Start();
    void Stop();
}
