package CAALHP.Contracts;

import CAALHP.SOAICE.Contracts.IServiceHostContractPrx;

/**
 * Created by rgst on 27-05-2014.
 */
public class ServiceHostToCAALHPContractAdapter implements IServiceHostCAALHPContract {

    private IHostCAALHPContract host;
    private final IServiceHostContractPrx serviceHostContract;

    public ServiceHostToCAALHPContractAdapter(IServiceHostContractPrx host)
    {
        System.out.println("Constructing ServiceHostToCAALHPContractAdapter");
        serviceHostContract = host;
        try
        {
            System.out.println("_serviceHostContract.GetHost");
            this.host = new HostToCAALHPContractAdapter(serviceHostContract);
        }
        catch (Exception ex)
        {
            System.out.println(ex);
        }
    }

    @Override
    public IHostCAALHPContract getHost() {
        return this.host;
    }

    @Override
    public void setHost(IHostCAALHPContract host) {
        this.host = host;
    }

    @Override
    public IPluginInfo[] GetListOfInstalledApps() {
        CAALHP.SOAICE.Contracts.IPluginInfo[] apps = serviceHostContract.GetListOfInstalledApps();
        return (IPluginInfo[]) apps;
    }

    @Override
    public IPluginInfo[] GetListOfInstalledDeviceDrivers() {
        CAALHP.SOAICE.Contracts.IPluginInfo[] drivers = serviceHostContract.GetListOfInstalledDeviceDrivers();
        return (IPluginInfo[]) drivers;
    }

    @Override
    public void CloseApp(String fileName) {
        serviceHostContract.CloseApp(fileName);
    }

    @Override
    public void ActivateDeviceDrivers() {
        serviceHostContract.ActivateDeviceDrivers();
    }

    @Override
    public String[] GetListOfEventTypes() {
        return serviceHostContract.GetListOfEventTypes();
    }
}
