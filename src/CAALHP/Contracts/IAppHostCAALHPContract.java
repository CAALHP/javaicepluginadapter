package CAALHP.Contracts;

/**
 * Created by rgst on 23-05-2014.
 */
public interface IAppHostCAALHPContract {
    IHostCAALHPContract getHost();
    void setHost(IHostCAALHPContract host);
    void CloseApp(String appName);
    IPluginInfo[] GetListOfInstalledApps();
    void ShowApp(String appName);
}
