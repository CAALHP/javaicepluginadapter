package CAALHP.Contracts;

/**
 * Created by rgst on 27-05-2014.
 */
public interface IDeviceDriverCAALHPContract extends IBaseCAALHPContract {
    void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId);
}
