package CAALHP.Contracts;

/**
 * Created by rgst on 27-05-2014.
 */
public interface IServiceHostCAALHPContract {
    IHostCAALHPContract getHost();
    void setHost(IHostCAALHPContract host);
    IPluginInfo[] GetListOfInstalledApps();
    IPluginInfo[] GetListOfInstalledDeviceDrivers();
    void CloseApp(String fileName);
    void ActivateDeviceDrivers();
    String[] GetListOfEventTypes();
}
