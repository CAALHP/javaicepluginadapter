package CAALHP.Contracts;

import java.util.AbstractMap;

/**
 * Created by rgst on 23-05-2014.
 */
public interface IHostCAALHPContract {
    void ReportEvent(AbstractMap.SimpleEntry<String, String> value);
    void SubscribeToEvents(String fullyQualifiedNameSpace, int processId);
    void UnSubscribeToEvents(String fullyQualifiedNameSpace, int processId);
}
