package CAALHP.Contracts;

/**
 * Created by rgst on 27-05-2014.
 */
public interface IDeviceDriverHostCAALHPContract {
    IHostCAALHPContract getHost();
    void setHost(IHostCAALHPContract host);
}
