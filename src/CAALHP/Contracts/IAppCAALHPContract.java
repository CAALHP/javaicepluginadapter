package CAALHP.Contracts;

/**
 * Created by rgst on 23-05-2014.
 */
public interface IAppCAALHPContract extends IBaseCAALHPContract {
    void Initialize(IAppHostCAALHPContract hostObj, int processId);
    void Show();
}
