package CAALHP.Contracts;

import CAALHP.SOAICE.Contracts.IAppHostContractPrx;

/**
 * Created by rgst on 23-05-2014.
 */
public class AppHostToCAALHPContractAdapter implements IAppHostCAALHPContract {
    private IHostCAALHPContract host;
    private IAppHostContractPrx appHostContract;

    public AppHostToCAALHPContractAdapter(IAppHostContractPrx host) {
        System.out.println("Constructing ServiceHostToCAALHPContractAdapter");
        this.appHostContract = host;
        try
        {
            System.out.println("_appHostContract.GetHost");
            this.host = new HostToCAALHPContractAdapter(this.appHostContract);
        }
        catch (Exception ex)
        {
            System.out.println(ex);
        }
    }

    @Override
    public IHostCAALHPContract getHost() {
        return this.host;
    }

    @Override
    public void setHost(IHostCAALHPContract host) {
        this.host = host;
    }

    @Override
    public void CloseApp(String appName) {
        appHostContract.CloseApp(appName);
    }

    @Override
    public IPluginInfo[] GetListOfInstalledApps() {
        CAALHP.SOAICE.Contracts.IPluginInfo[] apps = appHostContract.GetListOfInstalledApps();
        return (IPluginInfo[]) apps;
    }

    @Override
    public void ShowApp(String appName) {
        appHostContract.ShowApp(appName);
    }
}
