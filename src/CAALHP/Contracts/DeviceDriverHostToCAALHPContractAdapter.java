package CAALHP.Contracts;

import CAALHP.SOAICE.Contracts.IDeviceDriverHostContract;
import CAALHP.SOAICE.Contracts.IDeviceDriverHostContractPrx;

/**
 * Created by rgst on 27-05-2014.
 */
public class DeviceDriverHostToCAALHPContractAdapter implements IDeviceDriverHostCAALHPContract{

    private IHostCAALHPContract host;
    private IDeviceDriverHostContractPrx deviceDriverHostContract;

    public DeviceDriverHostToCAALHPContractAdapter(IDeviceDriverHostContractPrx host) {
        System.out.println("Constructing ServiceHostToCAALHPContractAdapter");
        deviceDriverHostContract = host;
        try
        {
            System.out.println("_deviceDriverHostContract.GetHost");
            this.host = new HostToCAALHPContractAdapter(deviceDriverHostContract);
        }
        catch (Exception ex)
        {
            System.out.println(ex);
        }
    }

    @Override
    public IHostCAALHPContract getHost() {
        return this.host;
    }

    @Override
    public void setHost(IHostCAALHPContract host) {

    }
}
