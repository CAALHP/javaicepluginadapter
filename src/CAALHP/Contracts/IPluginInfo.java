package CAALHP.Contracts;

/**
 * Created by rgst on 23-05-2014.
 */
public interface IPluginInfo {
    String getLocationDir();
    void setLocationDir(String locationDir);
    String getName();
    void setName(String name);
}
