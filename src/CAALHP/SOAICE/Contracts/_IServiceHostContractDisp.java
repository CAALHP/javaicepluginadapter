// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `CAALHP.SOAICE.Contracts.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package CAALHP.SOAICE.Contracts;

public abstract class _IServiceHostContractDisp extends Ice.ObjectImpl implements IServiceHostContract
{
    protected void
    ice_copyStateFrom(Ice.Object __obj)
        throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }

    public static final String[] __ids =
    {
        "::CAALHP::SOAICE::Contracts::IHostContract",
        "::CAALHP::SOAICE::Contracts::IServiceHostContract",
        "::Ice::Object"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    public final void Register(String name, String category, int processId)
    {
        Register(name, category, processId, null);
    }

    public final void ReportEvent(String key, String value)
    {
        ReportEvent(key, value, null);
    }

    public final void SubscribeToEvents(String fullyQualifiedNameSpace, int processId)
    {
        SubscribeToEvents(fullyQualifiedNameSpace, processId, null);
    }

    public final void UnSubscribeToEvents(String fullyQualifiedNameSpace, int processId)
    {
        UnSubscribeToEvents(fullyQualifiedNameSpace, processId, null);
    }

    public final void ActivateDeviceDrivers()
    {
        ActivateDeviceDrivers(null);
    }

    public final void CloseApp(String fileName)
    {
        CloseApp(fileName, null);
    }

    public final String[] GetListOfEventTypes()
    {
        return GetListOfEventTypes(null);
    }

    public final IPluginInfo[] GetListOfInstalledApps()
    {
        return GetListOfInstalledApps(null);
    }

    public final IPluginInfo[] GetListOfInstalledDeviceDrivers()
    {
        return GetListOfInstalledDeviceDrivers(null);
    }

    public static Ice.DispatchStatus ___GetListOfInstalledApps(IServiceHostContract __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        __inS.readEmptyParams();
        IPluginInfo[] __ret = __obj.GetListOfInstalledApps(__current);
        IceInternal.BasicStream __os = __inS.__startWriteParams(Ice.FormatType.DefaultFormat);
        IPluginInfoSeqHelper.write(__os, __ret);
        __inS.__endWriteParams(true);
        return Ice.DispatchStatus.DispatchOK;
    }

    public static Ice.DispatchStatus ___GetListOfInstalledDeviceDrivers(IServiceHostContract __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        __inS.readEmptyParams();
        IPluginInfo[] __ret = __obj.GetListOfInstalledDeviceDrivers(__current);
        IceInternal.BasicStream __os = __inS.__startWriteParams(Ice.FormatType.DefaultFormat);
        IPluginInfoSeqHelper.write(__os, __ret);
        __inS.__endWriteParams(true);
        return Ice.DispatchStatus.DispatchOK;
    }

    public static Ice.DispatchStatus ___CloseApp(IServiceHostContract __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.startReadParams();
        String fileName;
        fileName = __is.readString();
        __inS.endReadParams();
        __obj.CloseApp(fileName, __current);
        __inS.__writeEmptyParams();
        return Ice.DispatchStatus.DispatchOK;
    }

    public static Ice.DispatchStatus ___ActivateDeviceDrivers(IServiceHostContract __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        __inS.readEmptyParams();
        __obj.ActivateDeviceDrivers(__current);
        __inS.__writeEmptyParams();
        return Ice.DispatchStatus.DispatchOK;
    }

    public static Ice.DispatchStatus ___GetListOfEventTypes(IServiceHostContract __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        __inS.readEmptyParams();
        String[] __ret = __obj.GetListOfEventTypes(__current);
        IceInternal.BasicStream __os = __inS.__startWriteParams(Ice.FormatType.DefaultFormat);
        StringSeqHelper.write(__os, __ret);
        __inS.__endWriteParams(true);
        return Ice.DispatchStatus.DispatchOK;
    }

    private final static String[] __all =
    {
        "ActivateDeviceDrivers",
        "CloseApp",
        "GetListOfEventTypes",
        "GetListOfInstalledApps",
        "GetListOfInstalledDeviceDrivers",
        "Register",
        "ReportEvent",
        "SubscribeToEvents",
        "UnSubscribeToEvents",
        "ice_id",
        "ice_ids",
        "ice_isA",
        "ice_ping"
    };

    public Ice.DispatchStatus __dispatch(IceInternal.Incoming in, Ice.Current __current)
    {
        int pos = java.util.Arrays.binarySearch(__all, __current.operation);
        if(pos < 0)
        {
            throw new Ice.OperationNotExistException(__current.id, __current.facet, __current.operation);
        }

        switch(pos)
        {
            case 0:
            {
                return ___ActivateDeviceDrivers(this, in, __current);
            }
            case 1:
            {
                return ___CloseApp(this, in, __current);
            }
            case 2:
            {
                return ___GetListOfEventTypes(this, in, __current);
            }
            case 3:
            {
                return ___GetListOfInstalledApps(this, in, __current);
            }
            case 4:
            {
                return ___GetListOfInstalledDeviceDrivers(this, in, __current);
            }
            case 5:
            {
                return _IHostContractDisp.___Register(this, in, __current);
            }
            case 6:
            {
                return _IHostContractDisp.___ReportEvent(this, in, __current);
            }
            case 7:
            {
                return _IHostContractDisp.___SubscribeToEvents(this, in, __current);
            }
            case 8:
            {
                return _IHostContractDisp.___UnSubscribeToEvents(this, in, __current);
            }
            case 9:
            {
                return ___ice_id(this, in, __current);
            }
            case 10:
            {
                return ___ice_ids(this, in, __current);
            }
            case 11:
            {
                return ___ice_isA(this, in, __current);
            }
            case 12:
            {
                return ___ice_ping(this, in, __current);
            }
        }

        assert(false);
        throw new Ice.OperationNotExistException(__current.id, __current.facet, __current.operation);
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, true);
        __os.endWriteSlice();
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.endReadSlice();
    }

    public static final long serialVersionUID = 0L;
}
