package CAALHP.SOAICE.Contracts;

import Ice.Current;

/**
 * Created by rgst on 26-05-2014.
 */
public class ServiceAdapter extends _IServiceContractDisp {
    @Override
    public void Start(Current __current) {

    }

    @Override
    public void Stop(Current __current) {

    }

    @Override
    public String GetName(Current __current) {
        return null;
    }

    @Override
    public boolean IsAlive(Current __current) {
        return false;
    }

    @Override
    public void ShutDown(Current __current) {

    }

    @Override
    public void Notify(String key, String value, Current __current) {

    }
}
