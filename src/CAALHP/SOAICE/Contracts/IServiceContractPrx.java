// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `CAALHP.SOAICE.Contracts.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package CAALHP.SOAICE.Contracts;

public interface IServiceContractPrx extends IPluginBaseContractPrx
{
    public void Start();

    public void Start(java.util.Map<String, String> __ctx);

    public Ice.AsyncResult begin_Start();

    public Ice.AsyncResult begin_Start(java.util.Map<String, String> __ctx);

    public Ice.AsyncResult begin_Start(Ice.Callback __cb);

    public Ice.AsyncResult begin_Start(java.util.Map<String, String> __ctx, Ice.Callback __cb);

    public Ice.AsyncResult begin_Start(Callback_IServiceContract_Start __cb);

    public Ice.AsyncResult begin_Start(java.util.Map<String, String> __ctx, Callback_IServiceContract_Start __cb);

    public void end_Start(Ice.AsyncResult __result);

    public void Stop();

    public void Stop(java.util.Map<String, String> __ctx);

    public Ice.AsyncResult begin_Stop();

    public Ice.AsyncResult begin_Stop(java.util.Map<String, String> __ctx);

    public Ice.AsyncResult begin_Stop(Ice.Callback __cb);

    public Ice.AsyncResult begin_Stop(java.util.Map<String, String> __ctx, Ice.Callback __cb);

    public Ice.AsyncResult begin_Stop(Callback_IServiceContract_Stop __cb);

    public Ice.AsyncResult begin_Stop(java.util.Map<String, String> __ctx, Callback_IServiceContract_Stop __cb);

    public void end_Stop(Ice.AsyncResult __result);
}
