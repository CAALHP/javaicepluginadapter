// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `CAALHP.SOAICE.Contracts.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package CAALHP.SOAICE.Contracts;

public final class IDeviceDriverContractHolder extends Ice.ObjectHolderBase<IDeviceDriverContract>
{
    public
    IDeviceDriverContractHolder()
    {
    }

    public
    IDeviceDriverContractHolder(IDeviceDriverContract value)
    {
        this.value = value;
    }

    public void
    patch(Ice.Object v)
    {
        if(v == null || v instanceof IDeviceDriverContract)
        {
            value = (IDeviceDriverContract)v;
        }
        else
        {
            IceInternal.Ex.throwUOE(type(), v);
        }
    }

    public String
    type()
    {
        return _IDeviceDriverContractDisp.ice_staticId();
    }
}
