package CAALHP;

import CAALHP.Contracts.IServiceCAALHPContract;
import CAALHP.Contracts.ServiceHostToCAALHPContractAdapter;
import CAALHP.SOAICE.Contracts.IServiceHostContractPrx;
import CAALHP.SOAICE.Contracts.IServiceHostContractPrxHelper;
import CAALHP.SOAICE.Contracts._IServiceContractDisp;
import Ice.ConnectFailedException;
import Ice.Current;
import Ice.ObjectAdapter;
import Ice.ObjectPrx;

import java.io.Console;
import java.lang.management.ManagementFactory;
import java.util.AbstractMap;
import java.util.UUID;

/**
 * Created by rgst on 27-05-2014.
 */
public class ServiceAdapter extends _IServiceContractDisp {

    private Ice.Communicator ic;
    private Ice.Identity ident;
    private IServiceHostContractPrx hostProxy;
    private final IServiceCAALHPContract serviceContract;

    public ServiceAdapter(String endpoint, IServiceCAALHPContract serviceContract)
    {
        this.serviceContract = serviceContract;
        Connect(endpoint);
        boolean attachedSuccessfully = WaitForAttachment();
        if (!attachedSuccessfully)
        {
            throw new ConnectFailedException();
        }
    }

    /// <summary>
    /// The connect application provides CareStore Apps and Drivers the ability to connect to the CAALHP CareStoreNameService.
    /// Thus, it will register with the CAALPH through the CareStoreNameService, and thus allow CAALHP to communicate cross-process
    /// </summary>
    /// <param name="endpoint">the endpoint to call</param>
    private void Connect(final String endpoint)
    {
        ic = null;
        try
        {
            final ServiceAdapter service = this;

            Runnable task = new Runnable() {
                @Override
                public void run() {
                    // Create a communicator
                    ic = Ice.Util.initialize();
                    String address = endpoint;
                    if (endpoint == null || endpoint.isEmpty())
                        address = "127.0.0.1";

                    // Create a proxy
                    ObjectPrx obj = ic.stringToProxy("ServiceHost:default -h " + endpoint + " -p 10001");

                    // Down-cast the proxy to a CareStoreNameService proxy
                    hostProxy = IServiceHostContractPrxHelper.checkedCast(obj);

                    //Create a nameless adapter for holding the
                    ObjectAdapter adapter = ic.createObjectAdapter("");

                    //Create an identity for us to track the callback object
                    ident = new Ice.Identity(UUID.randomUUID().toString(), "Service");

                    //bind the identity with the implementation and activate it
                    adapter.add(service, ident);
                    adapter.activate();

                    //activate the adapter for the callback
                    hostProxy.ice_getConnection().setAdapter(adapter);

                    int pid = Integer.parseInt(ManagementFactory.getRuntimeMXBean().getName().split("@")[0]);

                    //register the callback with the NameService object
                    hostProxy.Register(ident.name, ident.category, pid);
                    System.out.println("Registered Service");
                    ServiceHostToCAALHPContractAdapter serviceHostToCAALHPContractAdapter = new ServiceHostToCAALHPContractAdapter(hostProxy);
                    System.out.println("Created servicehostadapter");
                    serviceContract.Initialize(serviceHostToCAALHPContractAdapter, pid);
                    System.out.println("Initialized Service");
                    ic.waitForShutdown();
                }

            };
                new Thread(task).start();
            }
        catch (ConnectFailedException e)
        {
            System.err.println(e);
            //status = 1;
        }
        if (ic == null) return;
        // Clean up
        try
        {
            ic.destroy();
        }
        catch (java.lang.Exception e)
        {
            System.err.println(e);
            //status = 1;
        }
        //Environment.Exit(status);
    }

    private boolean WaitForAttachment()
    {
        int counter = 0;

        while (!ConnectionEstablished() && counter < 50)
        {
            counter++;
            if(counter%10 == 1)
                System.out.println("Waiting for attachment to Service Host");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return ConnectionEstablished();
    }

    private boolean ConnectionEstablished()
    {
        return hostProxy != null && ident != null;
    }

    @Override
    public void Start(Current __current) {

    }

    @Override
    public void Stop(Current __current) {

    }

    @Override
    public String GetName(Current __current) {
        return serviceContract.GetName();
    }

    @Override
    public boolean IsAlive(Current __current) {
        return serviceContract.IsAlive();
    }

    @Override
    public void ShutDown(Current __current) {
        serviceContract.ShutDown();
    }

    @Override
    public void Notify(String key, String value, Current __current) {
        serviceContract.Notify(new AbstractMap.SimpleEntry<String, String>(key,value));
    }
}
