package CAALHP; /**
 * Created by rgst on 22-05-2014.
 */
import CAALHP.Contracts.AppHostToCAALHPContractAdapter;
import CAALHP.Contracts.IAppCAALHPContract;
import CAALHP.SOAICE.Contracts.IAppHostContractPrx;
import CAALHP.SOAICE.Contracts.IAppHostContractPrxHelper;
import CAALHP.SOAICE.Contracts._IAppContractDisp;
import Ice.Communicator;
import Ice.ConnectFailedException;
import Ice.Current;
import Ice.Identity;
import Ice.ObjectAdapter;
import Ice.ObjectPrx;

import java.lang.management.ManagementFactory;
import java.util.AbstractMap;
import java.util.UUID;

public class AppAdapter extends _IAppContractDisp {

    private final IAppCAALHPContract appContract;
    private Communicator ic;
    private Identity ident;
    private IAppHostContractPrx hostProxy;

    public AppAdapter(String endpoint, IAppCAALHPContract appContract){

        this.appContract = appContract;
        Connect(endpoint);
        boolean attachedSuccessfully = WaitForAttachment();
        if (!attachedSuccessfully)
        {
            throw new ConnectFailedException();
        }
    }

    private boolean WaitForAttachment() {
        int counter = 0;

        while (!ConnectionEstablished() && counter < 50)
        {
            counter++;
            if (counter % 10 == 1)
                System.out.println("Waiting for attachment to Service Host");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return ConnectionEstablished();
    }

    private boolean ConnectionEstablished() {
        return hostProxy != null && ident != null;
    }

    /**
     * The connect application provides CareStore Apps and Drivers the ability to connect to the CAALHP CareStoreNameService.
     * Thus, it will register with the CAALPH through the CareStoreNameService, and thus allow CAALHP to communicate cross-process
     * @param endpoint the endpoint to call
     */
    private void Connect(final String endpoint) {
        ic = null;
        try
        {
            final AppAdapter app = this;
            Runnable task = new Runnable() {
                @Override
                public void run() {

                        // Create a communicator
                        ic = Ice.Util.initialize();
                        String address = endpoint;
                        if (endpoint == null || endpoint.isEmpty())
                            address = "127.0.0.1";

                        // Create a proxy
                        ObjectPrx obj = ic.stringToProxy("AppHost:default -h " + endpoint + " -p 10003");

                        // Down-cast the proxy to a CareStoreNameService proxy
                        hostProxy = IAppHostContractPrxHelper.checkedCast(obj);

                        //Create a nameless adapter for holding the
                        ObjectAdapter adapter = ic.createObjectAdapter("");

                        //Create an identity for us to track the callback object
                        ident = new Ice.Identity(UUID.randomUUID().toString(), "App");

                        //bind the identity with the implementation and activate it
                        adapter.add(app, ident);
                        adapter.activate();

                        //activate the adapter for the callback
                        hostProxy.ice_getConnection().setAdapter(adapter);

                        int pid = Integer.parseInt(ManagementFactory.getRuntimeMXBean().getName().split("@")[0]);
                        //register the callback with the NameService object
                        hostProxy.Register(ident.name, ident.category, pid);
                        System.out.println("Registered Service");
                        AppHostToCAALHPContractAdapter appHostToCAALHPContractAdapter = new AppHostToCAALHPContractAdapter(hostProxy);
                        System.out.println("Created servicehostadapter");
                        appContract.Initialize(appHostToCAALHPContractAdapter, pid);
                        System.out.println("Initialized Service");
                        ic.waitForShutdown();
                    }

            };
            new Thread(task).start();

        }
        catch (ConnectFailedException e)
        {
            System.err.println(e);
            //status = 1;
        }
        if (ic == null) return;
        // Clean up
        try
        {
            ic.destroy();
        }
        catch (java.lang.Exception e)
        {
            System.err.println(e);
            //status = 1;
        }
        //Environment.Exit(status);
    }

    @Override
    public void Show(Current __current) {
        appContract.Show();
    }

    @Override
    public void Hide(Current __current) {

    }

    @Override
    public String GetName(Current __current) {
        return appContract.GetName();
    }

    @Override
    public boolean IsAlive(Current __current) {
        return appContract.IsAlive();
    }

    @Override
    public void ShutDown(Current __current) {
        appContract.ShutDown();
    }

    @Override
    public void Notify(String key, String value, Current __current) {
        appContract.Notify(new AbstractMap.SimpleEntry<String, String>(key,value));
    }

    public IAppHostContractPrx getHostProxy() {
        return hostProxy;
    }

    public void setHostProxy(IAppHostContractPrx hostProxy) {
        this.hostProxy = hostProxy;
    }
}
